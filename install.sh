#!/bin/bash

cd

echo "========================"
echo "= Umbrel GUI installer ="
echo "========================"
echo "===== Version 0.3 ======"
echo "========================"

echo "Installing build dependencies"
sudo apt install git libqt5gui5 libqt5svg5-dev libqt5network5 cmake make gcc g++ qtwebengine5-dev -y

echo "Downloading the GUI"
rm -rf umbrel-gui
git clone https://gitlab.com/UmbrelLabs/tools/umbrel-gui.git
cd umbrel-gui

echo "Creating build directory"
mkdir build && cd build

echo "Generating Makefile"
cmake ..

echo "Building"
make -j4

echo "Rebooting in 15 seconds"
echo "-----------------------"

echo "After the reboot is finished, the Raspberry Pi PIXEL desktop should open."
echo "There, you need to open a Terminal and run this:"
echo "umbrel-gui/build/umbrel-gui"

sleep 15
sudo reboot
